﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Usuarios.Aplicacao.Interfaces;
using Usuarios.Aplicacao.Requests;
using Usuarios.Dominio.Core.Bus;
using Usuarios.Dominio.Core.Notificacoes;
using Usuarios.Servico.Api.Controllers;

namespace Usuarios.Servicos.Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class UsuarioController : ApiController
    {
        private readonly IUsuarioApp _usuarioApp;

        public UsuarioController(IUsuarioApp usuarioApp,
            INotificationHandler<DomainNotification> notifications,
            IMediatorHandler mediator) : base(notifications, mediator)
        {
            _usuarioApp = usuarioApp;
        }

        [HttpGet]
        [Route("Obter")]
        public async Task<IActionResult> Get()
        {
            return Response(await _usuarioApp.Obter());
        }

        [HttpGet]
        [Route("ObterPorId/{id:int}")]
        public async Task<IActionResult> Get(int id)
        {
            var usuarioReponse = await _usuarioApp.ObterPorId(id);

            return Response(usuarioReponse);
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("Registrar")]
        public async Task<IActionResult> Post([FromBody]RegistraUsuarioRequest request)
        {
            if (!ModelState.IsValid)
            {
                await NotifyModelStateErrors();
                return Response(request);
            }

            var registroUsuario = await _usuarioApp.Registrar(request);

            return Response(registroUsuario);
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromBody]UsuarioRequest request)
        {
            if (!ModelState.IsValid)
            {
                await NotifyModelStateErrors();
                return Response(request);
            }

            await _usuarioApp.Atualizar(request);

            return Response(request);
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(int id)
        {
            await _usuarioApp.Remover(id);

            return Response();
        }

    }
}