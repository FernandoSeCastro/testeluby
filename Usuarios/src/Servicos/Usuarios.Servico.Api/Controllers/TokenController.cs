﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Usuarios.Aplicacao.Interfaces;
using Usuarios.Dominio.Core.Bus;
using Usuarios.Dominio.Core.Notificacoes;
using Usuarios.Servicos.Api.Controllers;
using Usuarios.Transversal.Autenticacao.Requests;

namespace Usuarios.Servico.Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class TokenController : ApiController
    {
        private readonly ITokenApp _tokenApp;

        public TokenController(ITokenApp tokenApp, INotificationHandler<DomainNotification> notifications, IMediatorHandler mediator) : base(notifications, mediator)
        {
            _tokenApp = tokenApp;
        }

        [HttpGet]
        [Route("Obter")]
        public async Task<IActionResult> Get([FromQuery]TokenRequest request)
        {
            return Response(await _tokenApp.Obter(request));
        }


    }
}