﻿using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Usuarios.Aplicacao.Requests;

namespace Usuarios.Servicos.Api.Filtros
{
    public class UsuarioAsyncActionFilter : IAsyncActionFilter
    {
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            if (context.HttpContext.User.Identity.IsAuthenticated)
            {
                foreach (var argument in context.ActionArguments.Values.Where(v => v is RequestBase))
                {
                    var model = argument as RequestBase;
                    model.ComUsuarioLogado(Convert.ToInt32(context.HttpContext.User.FindFirst("UsuarioId").Value));
                }
            }

            var resultContext = await next();
        }
    }
}
