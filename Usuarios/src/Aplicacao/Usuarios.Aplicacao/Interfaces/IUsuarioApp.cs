﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Usuarios.Aplicacao.Requests;
using Usuarios.Aplicacao.Responses;
using Usuarios.Aplicacao.Services;
using System.Runtime;

namespace Usuarios.Aplicacao.Interfaces
{
    public interface IUsuarioApp : IDisposable
    {
        Task<RegistroUsuarioResponse> Registrar(RegistraUsuarioRequest request);

        Task<IEnumerable<UsuarioResponse>> Obter();

        Task<UsuarioResponse> ObterPorId(int id);

        Task Atualizar(UsuarioRequest request);

        Task Remover(int id);
    }
}
