﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Usuarios.Transversal.Autenticacao.Requests;
using Usuarios.Transversal.Autenticacao.Responses;

namespace Usuarios.Aplicacao.Interfaces
{
    public interface ITokenApp
    {
        Task<TokenResponse> Obter(TokenRequest request);
    }
}
