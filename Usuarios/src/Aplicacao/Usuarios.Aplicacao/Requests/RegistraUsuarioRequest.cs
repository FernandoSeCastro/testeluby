﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Usuarios.Aplicacao.Requests
{
    public class RegistraUsuarioRequest
    {
        public string Nome { get; set; }

        public string Email { get; set; }

        public string Senha { get; set; }

        public DateTime DataNasc { get; set; }

        public string SecurityKey { get; set; }
    }
}
