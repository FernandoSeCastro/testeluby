﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Usuarios.Aplicacao.Requests
{
    public class UsuarioRequest: RequestBase
    {
        public int Id { get; set; }

        public string Nome { get; set; }

        public string Email { get; set; }

        public string Senha { get; set; }

        public DateTime DataNasc { get; set; }
    }
}
