﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Usuarios.Aplicacao.Requests
{
    public abstract class RequestBase
    {
        protected int UsuarioLogadoId { get; set; }

        public void ComUsuarioLogado(int usuarioLogadoId)
        {
            UsuarioLogadoId = usuarioLogadoId;
        }

    }
}
