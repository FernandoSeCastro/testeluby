﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Usuarios.Aplicacao.AutoMapper
{
    public class AutoMapperConfig
    {
        public static MapperConfiguration RegisterMappings()
        {
            return new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new DominioParaResponseMappingProfile());
                cfg.AddProfile(new RequestParaDominioMappingProfile());
            });
        }
    }
}
