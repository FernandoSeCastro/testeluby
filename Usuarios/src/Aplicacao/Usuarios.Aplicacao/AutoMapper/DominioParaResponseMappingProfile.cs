﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Usuarios.Aplicacao.Responses;
using Usuarios.Dominio.Modelos;

namespace Usuarios.Aplicacao.AutoMapper
{
    public class DominioParaResponseMappingProfile:Profile
    {
        public DominioParaResponseMappingProfile()
        {
            CreateMap<Usuario, UsuarioResponse>();
        }
    }
}
