﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Usuarios.Aplicacao.Requests;
using Usuarios.Aplicacao.Responses;
using Usuarios.Aplicacao.Services;
using Usuarios.Dominio.Comandos.Usuarios;
using Usuarios.Dominio.Modelos;

namespace Usuarios.Aplicacao.AutoMapper
{
    public class RequestParaDominioMappingProfile : Profile
    {
        public RequestParaDominioMappingProfile()
        {
            CreateMap<RegistraUsuarioRequest, RegistrarUsuarioComando>();
            CreateMap<UsuarioRequest, AtualizarUsuarioComando>();
        }
    }
}
