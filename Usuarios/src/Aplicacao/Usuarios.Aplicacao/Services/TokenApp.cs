﻿
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using Usuarios.Aplicacao.Interfaces;
using Usuarios.Dominio.Core.Bus;
using Usuarios.Dominio.Core.Notificacoes;
using Usuarios.Dominio.Interfaces.Repositores;
using Usuarios.Transversal.Autenticacao.Models;
using Usuarios.Transversal.Autenticacao.Requests;
using Usuarios.Transversal.Autenticacao.Responses;
using Usuarios.Transversal.Autenticacao.Servicos;

namespace Usuarios.Aplicacao.Services
{

    public class TokenApp : ITokenApp
    {
        private readonly IConfiguration _config;
        private readonly IUsuarioRepositorio _usuarioRepositorio;
        private readonly IMediatorHandler _bus;

        public TokenApp(IConfiguration configuration, IUsuarioRepositorio usuarioRepositorio,
            IMediatorHandler bus)
        {
            _config = configuration;
            _usuarioRepositorio = usuarioRepositorio;
            _bus = bus;
        }

        public async Task<TokenResponse> Obter(TokenRequest request)
        {
            if (request.SecurityKey != _config.GetSection("JwtSettings").GetSection("SecurityKey").Value)
            {
                await _bus.RaiseEvent(new DomainNotification("Login", "SecurityKey inválida"));
                return null;
            }

            var jwtSettings = new JwtSettings();
            new ConfigureFromConfigurationOptions<JwtSettings>(
                    _config.GetSection("JwtSettings"))
                .Configure(jwtSettings);

            var usuario = await _usuarioRepositorio.ObterPorEmailSenha(request.Email, request.Senha);

            if (usuario == null)
            {
                await _bus.RaiseEvent(new DomainNotification("Login", "Usuário e/ou senha incorreto"));
                return null;
            }

            List<Claim> claim = new List<Claim>();

            claim.Add(new Claim("UsuarioId", usuario.Id.ToString()));
            claim.Add(new Claim("Senha", usuario.Senha));
            claim.Add(new Claim(ClaimTypes.Role, "Aplicativo"));

            var token = JsonWebTokenServices.GenerateToken(jwtSettings, new ClaimsIdentity(
                new GenericIdentity(usuario.Id.ToString(), "UsuarioId"),
                claim.ToList()
            ));

            return token;
        }
    }
}
