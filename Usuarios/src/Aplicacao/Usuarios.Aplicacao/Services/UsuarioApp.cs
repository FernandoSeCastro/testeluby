﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Usuarios.Aplicacao.Interfaces;
using Usuarios.Aplicacao.Requests;
using Usuarios.Aplicacao.Responses;
using Usuarios.Dominio.Comandos.Usuarios;
using Usuarios.Dominio.Core.Bus;
using Usuarios.Dominio.Core.Notificacoes;
using Usuarios.Dominio.Interfaces.Repositores;
using Usuarios.Transversal.Autenticacao.Requests;

namespace Usuarios.Aplicacao.Services
{
    public class UsuarioApp : IUsuarioApp
    {
        private readonly IMapper _mapper;
        private readonly IUsuarioRepositorio _usuarioRepositorio;
        private readonly IMediatorHandler _bus;
        private readonly ITokenApp _tokenApp;
        private readonly DomainNotificationHandler _notifications;

        public UsuarioApp(IMapper mapper, ITokenApp tokenApp,
                                  IUsuarioRepositorio usuarioRepositorio,
                                  IMediatorHandler bus,
                                  INotificationHandler<DomainNotification> notifications)
        {
            _mapper = mapper;
            _usuarioRepositorio = usuarioRepositorio;
            _bus = bus;
            _tokenApp = tokenApp;
            _notifications = (DomainNotificationHandler)notifications;
        }


        public async Task Atualizar(UsuarioRequest request)
        {
            var comandoAtualizar = _mapper.Map<AtualizarUsuarioComando>(request);
            await _bus.SendCommand(comandoAtualizar);
        }

        public async Task<IEnumerable<UsuarioResponse>> Obter()
        {
            return (await _usuarioRepositorio.Obter()).ProjectTo<UsuarioResponse>(_mapper.ConfigurationProvider);
        }

        public async Task<UsuarioResponse> ObterPorId(int id)
        {
            return _mapper.Map<UsuarioResponse>(await _usuarioRepositorio.ObterPorId(id));
        }

        public async Task<RegistroUsuarioResponse> Registrar(RegistraUsuarioRequest request)
        {
            var comandoRegistrar = _mapper.Map<RegistrarUsuarioComando>(request);
            await _bus.SendCommand(comandoRegistrar);

            var response = new RegistroUsuarioResponse();

            if (_notifications.HasNotifications()) return response;

            var token = await _tokenApp.Obter(new TokenRequest()
            {
                Email = request.Email,
                Senha = request.Senha,
                SecurityKey = request.SecurityKey
            });

            response.Token = token;

            return response;
        }

        public async Task Remover(int id)
        {
            var comandoRemover = new RemoverUsuarioComando() { Id = id };
            await _bus.SendCommand(comandoRemover);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
