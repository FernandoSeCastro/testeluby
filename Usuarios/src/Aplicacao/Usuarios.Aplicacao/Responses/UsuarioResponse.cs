﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Usuarios.Aplicacao.Responses
{
    public class UsuarioResponse
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Senha { get; set; }
    }
}
