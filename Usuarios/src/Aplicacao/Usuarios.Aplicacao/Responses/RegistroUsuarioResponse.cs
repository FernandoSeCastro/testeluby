﻿using System;
using System.Collections.Generic;
using System.Text;
using Usuarios.Transversal.Autenticacao.Responses;

namespace Usuarios.Aplicacao.Responses
{
    public class RegistroUsuarioResponse
    {
        public TokenResponse Token { get; set; }

        public RegistroUsuarioResponse()
        {
            Token = new TokenResponse();
        }
    }
}
