﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Usuarios.Transversal.Autenticacao.Requests
{
    public class TokenRequest
    {
        public string SecurityKey { get; set; }
        public string Email { get; set; }
        public string Senha { get; set; }

    }
}
