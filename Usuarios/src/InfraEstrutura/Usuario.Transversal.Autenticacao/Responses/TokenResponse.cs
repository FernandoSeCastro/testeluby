﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Usuarios.Transversal.Autenticacao.Responses
{

    public class TokenResponse
    {
        public string accessToken;
        public DateTime expiration;

        public TokenResponse()
        {

        }

        public TokenResponse(string _accessToken, DateTime _expiration)
        {
            accessToken = _accessToken;
            expiration = _expiration;
        }

    }
}
