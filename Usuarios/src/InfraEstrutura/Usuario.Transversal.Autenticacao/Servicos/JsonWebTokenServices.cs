﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Usuarios.Transversal.Autenticacao.Models;
using Usuarios.Transversal.Autenticacao.Responses;

namespace Usuarios.Transversal.Autenticacao.Servicos
{
    public static class JsonWebTokenServices
    {
        public static TokenResponse GenerateToken(JwtSettings options, ClaimsIdentity _identity)
        {
            var utcNow = DateTime.UtcNow;
            var expiration = DateTime.UtcNow.AddMinutes(options.ExpirationMinutes);
            var idleTimeout = options.IdleTimeoutMinutes * 60 * 1000;

            var identity = _identity;

            var handler = new JwtSecurityTokenHandler();

            var securityToken = handler.CreateJwtSecurityToken(new SecurityTokenDescriptor
            {
                Issuer = options.Issuer,
                Audience = options.Audience,
                SigningCredentials = options.SigningCredentials,
                Subject = identity,
                IssuedAt = utcNow,
                NotBefore = utcNow,
                Expires = expiration
            });

            var accessToken = handler.WriteToken(securityToken);

            return new TokenResponse(accessToken, expiration);
        }

    }
}
