﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Usuarios.Dominio.Interfaces.Repositores;
using Usuarios.Dominio.Modelos;
using Usuarios.InfraEstrutura.ContextoConfig;

namespace Usuarios.InfraEstrutura.Repositorios
{
    public class UsuarioRepositorio : RepositorioGenerico<Usuario>, IUsuarioRepositorio
    {
        public UsuarioRepositorio(ContextConfig context) : base(context)
        {
        }
        public override async Task<Usuario> ObterPorId(int id)
        {
            return await DbSet.Include(u => u.Pessoa).FirstOrDefaultAsync(u => u.Id == id);
        }

        public async Task<Usuario> ObterPorEmail(string email)
        {
            return await Task.Run(() => DbSet.FirstOrDefault(x => x.Email.Equals(email, StringComparison.InvariantCultureIgnoreCase)));
        }

        public async Task<Usuario> ObterPorEmailSenha(string email, string senha)
        {
            return await Task.Run(() => DbSet.FirstOrDefault(x => x.Email.Equals(email, StringComparison.InvariantCultureIgnoreCase)
             && x.Senha == senha));
        }
    }
}
