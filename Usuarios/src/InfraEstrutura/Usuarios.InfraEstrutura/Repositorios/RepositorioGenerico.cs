﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Usuarios.Dominio.Core.Modelos;
using Usuarios.Dominio.Interfaces;
using Microsoft.EntityFrameworkCore;
using Usuarios.InfraEstrutura.ContextoConfig;

namespace Usuarios.InfraEstrutura.Repositorios
{
    public class RepositorioGenerico<TEntidade> : IRepositorio<TEntidade>, IDisposable where TEntidade : class
    {

        protected readonly ContextConfig Db;
        protected readonly DbSet<TEntidade> DbSet;

        public RepositorioGenerico(ContextConfig context)
        {
            Db = context;
            DbSet = Db.Set<TEntidade>();
        }

        public virtual async Task Adicionar(TEntidade obj)
        {
            await DbSet.AddAsync(obj);
        }

        public virtual async Task Atualizar(TEntidade obj)
        {
            await Task.Run(() => DbSet.Update(obj));
        }

        public void Dispose()
        {
            Db.Dispose();
            GC.SuppressFinalize(this);
        }

        public virtual async Task<IQueryable<TEntidade>> Obter()
        {
            return await Task.FromResult(DbSet.AsQueryable());
        }

        public virtual async Task<TEntidade> ObterPorId(int id)
        {
            return await DbSet.FindAsync(id);
        }

        public virtual async Task Remover(int id)
        {
            await Task.Run(async () => DbSet.Remove(await DbSet.FindAsync(id)));
        }

        public async Task<int> SalvarMudancas()
        {
            return await Db.SaveChangesAsync();
        }
    }
}
