﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Usuarios.Dominio.Interfaces;
using Usuarios.InfraEstrutura.ContextoConfig;

namespace Usuarios.InfraEstrutura.Uow
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ContextConfig _context;

        public UnitOfWork(ContextConfig context)
        {
            _context = context;
        }

        public async Task<bool> Commit()
        {
            return (await _context.SaveChangesAsync()) > 0;
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
