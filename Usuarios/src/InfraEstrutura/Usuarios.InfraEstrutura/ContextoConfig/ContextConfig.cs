﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Usuarios.Dominio.Modelos;

namespace Usuarios.InfraEstrutura.ContextoConfig
{
    public class ContextConfig: DbContext
    {
        public ContextConfig(DbContextOptions<ContextConfig> options) : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Usuario>()
            .HasOne(p => p.Pessoa)
            .WithOne(i => i.Usuario)
            .HasForeignKey<Pessoa>(b => b.Id);
        }
        public DbSet<Usuario> usuarios { get; set; }
        public DbSet<Pessoa> pessoas { get; set; }


    }
}
