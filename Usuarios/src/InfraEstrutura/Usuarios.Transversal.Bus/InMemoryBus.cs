﻿using MediatR;
using System;
using System.Threading.Tasks;
using Usuarios.Dominio.Core.Bus;
using Usuarios.Dominio.Core.Comandos;
using Usuarios.Dominio.Core.Eventos;

namespace Usuarios.Transversal.Bus
{
    public class InMemoryBus : IMediatorHandler
    {
        private readonly IMediator _mediator;

        public InMemoryBus(IMediator mediator)
        {
            _mediator = mediator;
        }

        public async Task<bool> SendCommand<T>(T command) where T : Comando
        {
            return await _mediator.Send(command);
        }

        public async Task RaiseEvent<T>(T @event) where T : Evento
        {
            await _mediator.Publish(@event);
        }
    }
}
