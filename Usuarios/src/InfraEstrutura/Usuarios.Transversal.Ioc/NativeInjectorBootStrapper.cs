﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using System;
using Usuarios.Aplicacao.Interfaces;
using Usuarios.Aplicacao.Services;
using Usuarios.Dominio.Comandos.Usuarios;
using Usuarios.Dominio.ComandosPrincipais;
using Usuarios.Dominio.Core.Bus;
using Usuarios.Dominio.Core.Notificacoes;
using Usuarios.Dominio.Eventos.Usuarios;
using Usuarios.Dominio.EventosPrincipais;
using Usuarios.Dominio.Interfaces;
using Usuarios.Dominio.Interfaces.Repositores;
using Usuarios.Dominio.Modelos;
using Usuarios.InfraEstrutura.ContextoConfig;
using Usuarios.InfraEstrutura.Repositorios;
using Usuarios.InfraEstrutura.Uow;
using Usuarios.Transversal.Bus;

namespace Usuarios.Transversal.Ioc
{
    public class NativeInjectorBootStrapper
    {
        public static void RegisterServices(IServiceCollection services)
        {
            // ASP.NET HttpContext dependency
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            // Domain Bus (Mediator)
            services.AddScoped<IMediatorHandler, InMemoryBus>();
            
            // Application
            services.AddScoped<IUsuarioApp, UsuarioApp>();
            services.AddScoped<ITokenApp, TokenApp>();

            // Domain - Events
            services.AddScoped<INotificationHandler<DomainNotification>, DomainNotificationHandler>();
            services.AddScoped<INotificationHandler<UsuarioRegistradoEvento>, UsuarioEventoHandler>();
            services.AddScoped<INotificationHandler<UsuarioAtualizadoEvento>, UsuarioEventoHandler>();
            services.AddScoped<INotificationHandler<UsuarioRemovidoEvento>, UsuarioEventoHandler>();


            // Domain - Commands
            services.AddScoped<IRequestHandler<RegistrarUsuarioComando, bool>, UsuarioCommandHandler>();
            services.AddScoped<IRequestHandler<AtualizarUsuarioComando, bool>, UsuarioCommandHandler>();
            services.AddScoped<IRequestHandler<RemoverUsuarioComando, bool>, UsuarioCommandHandler>();

            
            // Infra - Data
            services.AddScoped<IUsuarioRepositorio, UsuarioRepositorio>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<ContextConfig>();
        }
    }
}
