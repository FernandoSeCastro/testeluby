﻿using System;
using System.Collections.Generic;
using System.Text;
using Usuarios.Dominio.Core.Modelos;

namespace Usuarios.Dominio.Modelos
{   
    public class Usuario : Entidade
    {
        public string Email { get; set; }
        public string Senha { get; set; }
        public DateTime DataRegistro { get; set; }
        public bool Ativo { get; set; }
        public Pessoa Pessoa { get; set; }
        public virtual int IdPessoa { get; set; }

        public void ComPessoa(Pessoa pessoa)
        {
            Pessoa = pessoa;
            IdPessoa = pessoa.Id;
        }
        public void Ativar()
        {
            Ativo = true;
        }
    }
}
