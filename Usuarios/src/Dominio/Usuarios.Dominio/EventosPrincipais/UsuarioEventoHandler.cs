﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Usuarios.Dominio.Eventos.Usuarios;

namespace Usuarios.Dominio.EventosPrincipais
{
    public class UsuarioEventoHandler :
        INotificationHandler<UsuarioRegistradoEvento>,
        INotificationHandler<UsuarioAtualizadoEvento>,
        INotificationHandler<UsuarioRemovidoEvento>
    {
        public Task Handle(UsuarioRegistradoEvento notification, CancellationToken cancellationToken)
        {
            // Send some notification e-mail

            return Task.CompletedTask;
        }

        public Task Handle(UsuarioAtualizadoEvento notification, CancellationToken cancellationToken)
        {
            // Send some greetings e-mail

            return Task.CompletedTask;
        }

        public Task Handle(UsuarioRemovidoEvento notification, CancellationToken cancellationToken)
        {
            // Send some see you soon e-mail

            return Task.CompletedTask;
        }
    }
}
