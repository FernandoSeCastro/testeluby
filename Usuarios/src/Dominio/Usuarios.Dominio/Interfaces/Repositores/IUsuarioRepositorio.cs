﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Usuarios.Dominio.Modelos;

namespace Usuarios.Dominio.Interfaces.Repositores
{
    public interface IUsuarioRepositorio : IRepositorio<Usuario>
    {
        Task<Usuario> ObterPorEmail(string email);
        Task<Usuario> ObterPorEmailSenha(string email, string senha);
    }
}
