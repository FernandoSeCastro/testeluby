﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Usuarios.Dominio.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        Task<bool> Commit();
    }
}
