﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Usuarios.Dominio.Interfaces
{
    public interface IRepositorio<TEntidade> : IDisposable where TEntidade : class
    {
        Task Adicionar(TEntidade obj);
        Task<TEntidade> ObterPorId(int id);
        Task<IQueryable<TEntidade>> Obter();
        Task Atualizar(TEntidade obj);
        Task Remover(int id);
        Task<int> SalvarMudancas();
    }
}
