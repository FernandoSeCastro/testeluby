﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Usuarios.Dominio.Comandos.Usuarios;
using Usuarios.Dominio.Core.Bus;
using Usuarios.Dominio.Core.Notificacoes;
using Usuarios.Dominio.Eventos.Usuarios;
using Usuarios.Dominio.Interfaces;
using Usuarios.Dominio.Interfaces.Repositores;
using Usuarios.Dominio.Modelos;

namespace Usuarios.Dominio.ComandosPrincipais
{
    public class UsuarioCommandHandler : CommandHandler,
        IRequestHandler<RegistrarUsuarioComando, bool>,
        IRequestHandler<AtualizarUsuarioComando, bool>,
        IRequestHandler<RemoverUsuarioComando, bool>
    {
        private readonly IUsuarioRepositorio _usuarioRepositorio;
        private readonly IMediatorHandler Bus;

        public UsuarioCommandHandler(IUsuarioRepositorio usuarioRepositorio,
            IUnitOfWork uow,
            IMediatorHandler bus,
            INotificationHandler<DomainNotification> notifications) : base(uow, bus, notifications)
        {
            _usuarioRepositorio = usuarioRepositorio;
            Bus = bus;
        }

        public async Task<bool> Handle(RegistrarUsuarioComando request, CancellationToken cancellationToken)
        {
            if (!request.IsValid())
            {
                await NotifyValidationErrors(request);
                return false;
            }

            var usuarioEmail = await _usuarioRepositorio.ObterPorEmail(request.Email);

            if (usuarioEmail != null)
            {
                await Bus.RaiseEvent(new DomainNotification(request.MessageType, "Esse e-mail já está cadastrado"));
                return false;
            }

            var usuario = new Usuario()
            {
                Senha = request.Senha,
                Email = request.Email,
            };

            usuario.ComPessoa(new Pessoa()
            {
                Nome = request.Nome,
                DataNasc = request.DataNasc
            });

            usuario.Ativar();

            await _usuarioRepositorio.Adicionar(usuario);

            if (await Commit())
            {
                await Bus.RaiseEvent(new UsuarioRegistradoEvento() { Id = usuario.Id, Nome = usuario.Pessoa.Nome, Email = usuario.Email });
            }

            return true;
        }

        public async Task<bool> Handle(AtualizarUsuarioComando request, CancellationToken cancellationToken)
        {
            if (!request.IsValid())
            {
                await NotifyValidationErrors(request);
                return false;
            }

            var usuario = await _usuarioRepositorio.ObterPorId(request.Id);

            if (usuario == null)
            {
                await Bus.RaiseEvent(new DomainNotification(request.MessageType, "Usuário não foi encontrado"));
                return false;
            }

            var usuarioEmail = await _usuarioRepositorio.ObterPorEmail(request.Email);


            if (usuarioEmail != null && usuario.Id != usuarioEmail.Id)
            {
                await Bus.RaiseEvent(new DomainNotification(request.MessageType, "Esse e-mail já está cadastrado"));
                return false;
            }

            usuario.Email = request.Email;
            usuario.Pessoa.Nome = request.Nome;
            usuario.Pessoa.DataNasc = request.DataNasc;

            await _usuarioRepositorio.Atualizar(usuario);

            if (await Commit())
            {
                await Bus.RaiseEvent(new UsuarioAtualizadoEvento() { Id = usuario.Id, Nome = usuario.Pessoa.Nome, Email = usuario.Email });
            }

            return true;
        }

        public async Task<bool> Handle(RemoverUsuarioComando request, CancellationToken cancellationToken)
        {
            if (!request.IsValid())
            {
                await NotifyValidationErrors(request);
                return false;
            }

            await _usuarioRepositorio.Remover(request.Id);

            if (await Commit())
            {
                await Bus.RaiseEvent(new UsuarioRemovidoEvento() { Id = request.Id });
            }

            return true;
        }
    
    }
}
