﻿using MediatR;
using Usuarios.Dominio.Core.Bus;
using Usuarios.Dominio.Core.Comandos;
using Usuarios.Dominio.Core.Notificacoes;
using Usuarios.Dominio.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Usuarios.Dominio.ComandosPrincipais
{
    public class CommandHandler
    {
        private readonly IUnitOfWork _uow;
        private readonly IMediatorHandler _bus;
        private readonly DomainNotificationHandler _notifications;

        public CommandHandler(IUnitOfWork uow, IMediatorHandler bus, INotificationHandler<DomainNotification> notifications)
        {
            _uow = uow;
            _notifications = (DomainNotificationHandler)notifications;
            _bus = bus;
        }

        protected async Task NotifyValidationErrors(Comando message)
        {
            foreach (var error in message.ValidationResult.Errors)
            {
                await _bus.RaiseEvent(new DomainNotification(message.MessageType, error.ErrorMessage));
            }
        }

        public async Task<bool> Commit()
        {
            if (_notifications.HasNotifications()) return false;
            if (await _uow.Commit()) return true;

            await _bus.RaiseEvent(new DomainNotification("Commit", "Tivemos problemas com o processamento dos dados."));
            return false;
        }
    }
}
