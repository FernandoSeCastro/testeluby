﻿using System;
using System.Collections.Generic;
using System.Text;
using Usuarios.Dominio.Core.Eventos;

namespace Usuarios.Dominio.Eventos.Usuarios
{
    public class UsuarioAtualizadoEvento: Evento
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Senha { get; set; }
        public bool Ativo { get; set; }
        public string CampoNulo { get; set; }
    }
}
