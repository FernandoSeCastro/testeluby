﻿using System;
using System.Collections.Generic;
using System.Text;
using Usuarios.Dominio.Core.Eventos;

namespace Usuarios.Dominio.Eventos.Usuarios
{
    public class UsuarioRemovidoEvento:Evento
    {
        public int Id { get; set; }
    }
}
