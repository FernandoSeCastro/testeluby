﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;
using Usuarios.Dominio.Comandos.Usuarios;

namespace Usuarios.Dominio.Validacoes.Usuarios
{
    public class UsuarioValidacao<T> : AbstractValidator<T> where T : UsuarioComando
    {
        protected void ValidarNome()
        {
            RuleFor(c => c.Nome)
                .NotEmpty().WithMessage("Informe o nome")
                .Length(5, 150).WithMessage("O nome deve ter entre 5 e 100 caracteres");
        }

        protected void ValidarSenha()
        {
            RuleFor(c => c.Senha)
                .NotEmpty().WithMessage("Informe a senha")
                .Length(8, 20).WithMessage("A senha deve ter entre 8 e 20 caracteres");
        }

        protected void ValidarEmail()
        {
            RuleFor(c => c.Email)
                .NotEmpty()
                .EmailAddress();
        }

        protected void ValidarId()
        {
            /*RuleFor(c => c.Id)
                .NotEqual(0);*/
        }

    }
}
