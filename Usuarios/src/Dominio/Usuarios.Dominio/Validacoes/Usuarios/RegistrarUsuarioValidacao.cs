﻿using System;
using System.Collections.Generic;
using System.Text;
using Usuarios.Dominio.Comandos.Usuarios;

namespace Usuarios.Dominio.Validacoes.Usuarios
{
    public class RegistrarUsuarioValidacao : UsuarioValidacao<RegistrarUsuarioComando>
    {
        public RegistrarUsuarioValidacao()
        {
            ValidarId();
            ValidarEmail();
            ValidarNome();
            ValidarSenha();
        }
    }
}
