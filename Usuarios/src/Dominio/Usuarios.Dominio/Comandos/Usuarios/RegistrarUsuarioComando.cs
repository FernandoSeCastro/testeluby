﻿using System;
using System.Collections.Generic;
using System.Text;
using Usuarios.Dominio.Validacoes.Usuarios;

namespace Usuarios.Dominio.Comandos.Usuarios
{
    public class RegistrarUsuarioComando : UsuarioComando
    {
        public override bool IsValid()
        {
            ValidationResult = new RegistrarUsuarioValidacao().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}
