﻿using System;
using System.Collections.Generic;
using System.Text;
using Usuarios.Dominio.Core.Comandos;

namespace Usuarios.Dominio.Comandos.Usuarios
{
    public abstract class UsuarioComando : Comando
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Senha { get; set; }
        public DateTime DataNasc { get; set; }
    }
}
