﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;
using Usuarios.Dominio.Validacoes.Usuarios;

namespace Usuarios.Dominio.Comandos.Usuarios
{
    public class RemoverUsuarioComando : UsuarioComando
    {
        public override bool IsValid()
        {
            ValidationResult = new RemoverUsuarioValidacao().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}
