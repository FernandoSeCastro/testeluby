﻿using System;
using System.Collections.Generic;
using System.Text;
using Usuarios.Dominio.Validacoes.Usuarios;

namespace Usuarios.Dominio.Comandos.Usuarios
{
    public class AtualizarUsuarioComando : UsuarioComando
    {
        public override bool IsValid()
        {
            ValidationResult = new AtualizarUsuarioValidacao().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}
