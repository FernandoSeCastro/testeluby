﻿using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Text;
using Usuarios.Dominio.Core.Eventos;

namespace Usuarios.Dominio.Core.Comandos
{
    public abstract class Comando : Mensagem
    {
        public DateTime Timestamp { get; private set; }
        public ValidationResult ValidationResult { get; set; }

        protected Comando()
        {
            Timestamp = DateTime.Now;
        }

        public abstract bool IsValid();
    }
}
