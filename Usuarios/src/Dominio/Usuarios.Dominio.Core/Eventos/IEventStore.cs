﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Usuarios.Dominio.Core.Eventos
{
    public interface IEventStore
    {
        void Save<T>(T theEvent) where T : Evento;
    }
}
