﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Usuarios.Dominio.Core.Eventos
{
    public abstract class Evento : Mensagem, INotification
    {
        public DateTime Timestamp { get; private set; }

        public Evento()
        {
            Timestamp = DateTime.Now;
        }
    }
}
