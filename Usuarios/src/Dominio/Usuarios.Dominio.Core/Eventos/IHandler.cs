﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Usuarios.Dominio.Core.Eventos
{
    public interface IHandler<in T> where T : Mensagem
    {
        void Handle(T message);
    }
}
