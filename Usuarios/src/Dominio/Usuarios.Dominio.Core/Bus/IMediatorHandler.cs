﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Usuarios.Dominio.Core.Comandos;
using Usuarios.Dominio.Core.Eventos;

namespace Usuarios.Dominio.Core.Bus
{    
    public interface IMediatorHandler
    {
        Task<bool> SendCommand<T>(T command) where T : Comando;
        Task RaiseEvent<T>(T @event) where T : Evento;
    }
}
